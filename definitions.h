


#define TRUE 1
#define FALSE 0

#define JOINT_AMOUNT 12
#define MAX_V 5.0f
#define VELOCITY 0.75f
#define FRICTION 0.9f
#define WIND 0.0f
#define MIN_V 0.05f