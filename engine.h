#ifndef ENGINE_H_
#define ENGINE_H_

void display();
void reshape(int width, int height);
void timer(int value);
void dispose();
void engine_init(int argc, char** argv, void (* renderfunction)());

int * getWindowWidth();
int * getWindowHeight();

#endif