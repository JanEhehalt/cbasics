#ifndef LINERENDERER_H_
#define LINERENDERER_H_

typedef struct LineRenderer {
    int * windowWidth;
    int * windowHeight;
    
    float lux;
    float luy;
    float rux;
    float ruy;
    float rox;
    float roy;    
    float lox;
    float loy;
} LineRenderer;

LineRenderer * linerendererInit();
void drawFilledRectangle(LineRenderer * renderer, int x, int y, int width, int height);
void drawLineRectangle(LineRenderer * renderer, int x, int y, int width, int height);
void drawLine(LineRenderer * renderer, int x_from, int y_from, int x_to, int y_to);


#endif