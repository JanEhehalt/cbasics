#include "linerenderer.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <stdlib.h>

void calcRectangleCorners(LineRenderer * renderer, int x, int y, int width, int height){
    int windowWidth = *renderer->windowWidth;
    int windowHeight = *renderer->windowHeight;

    if (width < 0){
        x += width;
        width = -width;
    }
    if (height < 0){
        y += height;
        height = -height;
    }
    
    renderer->lux = (float) x / (float)windowWidth * 2.0f  -1;
    renderer->luy = (float) y / (float)windowHeight * 2.0f -1;
    renderer->rux = ((float) (x + width) / (float) windowWidth) * 2.0f - 1;
    renderer->ruy = renderer->luy;
    renderer->rox = renderer->rux;
    renderer->roy = ((float) (y + height) / (float) windowHeight) * 2.0f - 1;
    renderer->lox = renderer->lux;
    renderer->loy = renderer->roy;
}

LineRenderer * linerendererInit(int * windowWidth, int * windowHeight){
    LineRenderer * instance = malloc(sizeof(LineRenderer));
    instance->windowWidth = windowWidth;
    instance->windowHeight = windowHeight;
    return instance;
}

void drawLineRectangle(LineRenderer * renderer, int x, int y, int width, int height){

    calcRectangleCorners(renderer, x, y, width, height);

    glBegin(GL_LINES);    
    //unten
    glVertex2f( renderer->lux, renderer->luy);
    glVertex2f( renderer->rux, renderer->ruy);

    //rechts
    glVertex2f(renderer->rux, renderer->ruy);
    glVertex2f(renderer->rox, renderer->roy);

    //oben
    glVertex2f(renderer->rox, renderer->roy);
    glVertex2f(renderer->lox, renderer->loy);

    //links
    glVertex2f(renderer->lox, renderer->loy);
    glVertex2f(renderer->lux ,renderer->luy);
    glEnd();

}

void drawFilledRectangle(LineRenderer * renderer, int x, int y, int width, int height){
    
    calcRectangleCorners(renderer, x, y, width, height);

    glRectf(renderer->lux, renderer->luy, renderer->rox, renderer->roy);
}

void drawLine(LineRenderer * renderer, int x_from, int y_from, int x_to, int y_to){
    int windowWidth = *renderer->windowWidth;
    int windowHeight = *renderer->windowHeight;
    float lux = (float) x_from /  (float) windowWidth   * 2.0f - 1;
    float luy = (float) y_from /  (float) windowHeight  * 2.0f - 1;
    float rox = ((float) (x_to) / (float) windowWidth)  * 2.0f - 1;
    float roy = ((float) (y_to) / (float) windowHeight) * 2.0f - 1;
    
    glBegin(GL_LINES);    
    glVertex2f( lux, luy);
    glVertex2f( rox, roy);
    glEnd();

}
