#include "color.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>    
       
void setColor(Color c){
    glColor3f(c.r/255.0f, c.g/255.0f, c.b/255.0f);
}
