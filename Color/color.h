#ifndef COLOR_H_
#define COLOR_H_

#define CLITERAL(type)      (type)
#define LIGHTGRAY  CLITERAL(Color){ 200, 200, 200, 255 } 
#define YELLOW     CLITERAL(Color){ 253, 249, 0, 255 }     
#define WHITE      CLITERAL(Color){ 255, 255, 255, 255 }   
#define BLACK      CLITERAL(Color){ 0, 0, 0, 255 }  

typedef struct Color {
    unsigned char r;        // Color red value
    unsigned char g;        // Color green value
    unsigned char b;        // Color blue value
    unsigned char a;        // Color alpha value
} Color;

void setColor(Color c);

#endif