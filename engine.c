#include "engine.h"
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <stdio.h>
#include <stdlib.h>

int W_WIDTH;
int W_HEIGHT;
void (* renderfunc)();

void engine_init(int argc, char** argv, void (* renderfunction)()){
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_SINGLE | GLUT_RGB);
    glutInitWindowSize(500, 500);
    glutCreateWindow("Cooles Programm");
    glutDisplayFunc(display);
    glutReshapeFunc(reshape);
    glutTimerFunc(0, timer, 0);
    renderfunc = renderfunction;
}

int * getWindowWidth(){
    return &W_WIDTH;
}
int * getWindowHeight(){
    return &W_HEIGHT;
}

void display()
{
    glClear(GL_COLOR_BUFFER_BIT);
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();

    renderfunc();
 
    glFlush();
    fflush(stdout);
}


void reshape(int width, int height)
{
    if(height > width) height = width;
    if(width > height) width = height;

    if (width < 500) width = 500;
    if (height < 500) height = 500;

    W_WIDTH = width;
    W_HEIGHT = height;
    
    glViewport(0, 0, width, height);
    
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluOrtho2D(-1.0, 1.0, -1.0 * (float)height / (float)width, 1.0 * (float)height / (float)width);
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
}

void timer(int value){
    glutPostRedisplay();
    glutTimerFunc(1000 / 60, timer, 0);
}
