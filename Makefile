CC = gcc
FLAGS = -lGL -lGLU -lglut -lm
OBJS = main.o color.o linerenderer.o engine.o

run: $(OBJS)
	$(CC) -o run $(OBJS) $(FLAGS)


main.o: main.c
	$(CC) -c main.c $(FLAGS)

color.o: Color/color.c
	$(CC) -c Color/color.c $(FLAGS)

linerenderer.o: LineRenderer/linerenderer.c
	$(CC) -c LineRenderer/linerenderer.c $(FLAGS)

engine.o: engine.c
	$(CC) -c engine.c $(FLAGS)


clean:
	rm *.o run