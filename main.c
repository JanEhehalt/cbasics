#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include <stdio.h>
#include <stdlib.h>
#include "Color/color.h"
#include "LineRenderer/linerenderer.h"
#include "engine.h"
#include "definitions.h"
#include <math.h>

typedef struct Joint {
    int is_solid;
    float x;
    float y;
    float v_x;
    float v_y;
} Joint;

Joint * joints[JOINT_AMOUNT];
int * windowWidth, * windowHeight;
LineRenderer * linerenderer;


float dist(Joint * j1, Joint * j2){
    float dx = j1->x - j2->x;
    float dy = j1->y - j2->y;
    return sqrtf((dx * dx) + (dy * dy));
}


void render(){
    glClearColor(1.0, 1.0, 1.0, 0.0);

    drawLineRectangle(linerenderer, 0, 0, 499, 499);

    setColor(BLACK);
    int lastx = 0;
    int lasty = 0;
    for(int i = 0; i < JOINT_AMOUNT; i++){
        unsigned char size = 5;
        if(joints[i]->is_solid == TRUE) size = 12;
        drawFilledRectangle(linerenderer, joints[i]->x- size/2, joints[i]->y- size/2, size, size);
        if (i > 0){
            drawLine(linerenderer, lastx, lasty, joints[i]->x, joints[i]->y);
        }
        lastx = joints[i]->x;
        lasty = joints[i]->y;
    }

    for(int i = 0; i < JOINT_AMOUNT; i++){
        Joint * j1 = joints[i];
        if(i > 0){
            Joint * j2 = joints[i-1];
            float maxDist = 25.0f;
            if(dist(j1, j2) > maxDist){
                float ratio =  1 - maxDist / dist(j1, j2);
                float i_x = (j2->x - j1->x) * ratio;
                float i_y = (j2->y - j1->y) * ratio;
                j1->v_x += i_x;
                j1->v_y += i_y;
                for(int j = 1; j < i; j++){
                    Joint * j3 = joints[j];
                    float impx = i_x * pow(0.1f, i-j);
                    float impy = i_y * pow(0.1f, i-j);
                    if(fabsf(impx) < MIN_V)impx = 0;
                    if(fabsf(impy) < MIN_V)impy = 0;
                    j3->v_x -= impx;
                    j3->v_y -= impy;
                }
            }
        }
        j1->v_x *= FRICTION;
        if(j1->is_solid == FALSE){
            j1->v_x += WIND;
            j1->v_x *= FRICTION;
            j1->v_y *= FRICTION;
            if(fabsf(j1->v_x) < MIN_V) j1->v_x = 0;
            if(fabsf(j1->v_y) < MIN_V) j1->v_y = 0;
            if(j1->v_y > MAX_V) j1->v_y = MAX_V;
            if(j1->v_x > MAX_V) j1->v_x = MAX_V;
            j1->x += j1->v_x;
            j1->y += j1->v_y;
            j1->v_y -= VELOCITY;
        }
    }

}

int main(int argc, char** argv)
{
    engine_init(argc, argv, render);

    for(int i = 0; i < JOINT_AMOUNT; i++){
        Joint * joint = malloc(sizeof(Joint));
        if (i == 0) joint->is_solid = TRUE;
        else joint->is_solid = FALSE;
        joint->x = 250 + i * 10;
        joint->y = 450 + i * 2;
        joint->v_x = 0;
        joint->v_y = 0;
        joints[i] = joint;
    }

    //printf("%f\n", dist(joints[0], joints[1]));
    
    
    windowWidth = getWindowWidth();
    windowHeight = getWindowHeight();
    linerenderer = linerendererInit(windowWidth, windowHeight);
    
    
    glutMainLoop();
    dispose();
    return 0;
}

void dispose(){
    printf("DISPOSE");
    free(linerenderer);
}


